import { LightningElement, track, api } from 'lwc';
import { registerListener, unregisterAllListeners } from "c/pubsub";

import fatchPickListValue from '@salesforce/apex/NH_ChildCareSchedulesHoursController.fatchPickListValue';

export default class MultiSelectDropDown extends LightningElement {
    @track showPicklist;
    @track statePicklistValues;
    @track selectedStatesArray = new Set();
    @track selectIdList = new Set();
    @track selectedStates = '';
    @track selectedId = '';
    @track pastStates;
    @track isAllSelected = false;
    @track pickValuesChild = [];
    @track fieldAriaLabel;


    @api objectName;
    @api fieldName;

    @api fieldLabel;

    //@api selectedValues;
    //@api picklistValues;
    @api customStyle = false;
    @api hasError = false;
    @api isPreview = false;

    @api
    get picklistValues() {
        return this.pastStates;
    }
    set picklistValues(picklistValues) {

        console.log('this.picklistValues', JSON.stringify(picklistValues));
        let picklists = [];
        picklists.push({ label: 'All', value: 'All' })
        for (let val of picklistValues) {
            //console.log('inside pickval');
            picklists.push({ label: val.label, value: val.value })
            //pickValueToLableMap.set(val.value ,val );
        }
        //console.log('pickValueToLableMap ',JSON.stringify(pickValueToLableMap));
        //console.log('picklistValues ',JSON.stringify(picklists));

        this.pastStates = picklists;
        if (this.selectedValues) {
            let selectedVals = [];
            for (var pickval of picklists) {
                if (this.selectedValues.includes(pickval.value)) {
                    selectedVals.push(pickval.value);
                }
            }
            this.selectedStatesArray = new Set();
            if (selectedVals.length > 0) {
                for (var value of selectedVals) {
                    let temp = picklists[picklists.findIndex(a => a.value == value)];
                    //console.log('temp ',JSON.stringify(temp));
                    this.selectedStatesArray.add(temp);
                }
                this.selectedStates = selectedVals.join(';');
            }

        }

        this.fieldAriaLabel = this.fieldLabel + ', Required';

        this.checkIsAllChecked();

    }

    @api
    get selectedValues() {
        return this.selectedStates;
    }
    set selectedValues(selectedValues) {
        this.selectedStates = selectedValues;
        /*
        let picklists = this.picklistValues;
        if (selectedValues) {
            this.selectedStatesArray = new Set();
            for (var value of selectedValues.split(';')) {
                let temp = picklists[picklists.findIndex(a => a.value == value)];
                //console.log('temp ',JSON.stringify(temp));
                this.selectedStatesArray.add(temp);
            }
            this.selectedStates = selectedValues;

        }

        this.checkIsAllChecked();
        */
    }

    handleShowPicklist() {

        if (!this.isPreview) {
            //console.log(this.showPicklist);
            this.showPicklist = !this.showPicklist;
            if (this.showPicklist) {
                this.dispatchEvent(new CustomEvent('show', {
                    detail: this.fieldName
                }));
            }
        }
    }

    @api
    hidePicklist() {
        this.showPicklist = false;
    }

    get parentTagStyle() {
        return !this.customStyle ? `slds-col slds-p-bottom_small slds-p-bottom_small slds-p-right_medium slds-size_1-of-1` : '';
    }

    get pillClass() {
        return this.hasError ? `slds-pill_container border slds-has-error slds-wrap slds-p-top_xx-small` : `slds-pill_container border slds-wrap slds-p-top_xx-small`;
    }


    handleSelectPicklist(event) {
        //console.log('log in parent');

        let selection = event.detail;

        // let selectIdList = [];

        this.showPicklist = false;
        if (this.pastStates[selection].label == 'All') {
            this.selectedStatesArray = new Set();
            for (var pick of this.pastStates) {
                //console.log(pick.label);
                //console.log(pick.label.length);
                if (pick.label != 'All') {
                    this.selectedStatesArray.add(pick);
                    // this.selectIdList.add(pick.value);
                }
            }
        } else {
            this.selectedStatesArray.add(this.pastStates[selection]);
            //this.selectIdList.add(this.pastStates[selection].value);
        }
        //console.log(this.selectedStatesArray);
        this.selectedStatesArray = new Set(this.selectedStatesArray);
        //console.log(this.selectedStatesArray);
        this.selectedStates = '';
        this.selectedId = '';

        for (var pick of this.selectedStatesArray) {
            if (this.selectedStates.length === 0) {
                this.selectedStates = this.selectedStates + pick.value;


            } else if (this.selectedStates.length > 0) {
                this.selectedStates = this.selectedStates + ';' + pick.value;
            }

        }

        // for (var pick of this.selectIdList) {
        //     if (this.selectedId.length === 0) {
        //         this.selectedId = this.selectedId + pick;


        //     } else if (this.selectedId.length > 0) {
        //         this.selectedId = this.selectedId + ';' + pick;
        //     }

        // }

        this.checkIsAllChecked();

        const updateFieldEvent = new CustomEvent('updatefielvalue', {
            detail: { fieldName: this.fieldName, fieldValue: /*JSON.stringify(*/this.selectedStates }
        });
        this.dispatchEvent(updateFieldEvent);
        this.showPicklist = true;


    }

    handleSelectPicklistRemove(event) {
        this.showPicklist = false;
        let toRemove = event.detail;

        if (toRemove == 'All') {
            this.selectedStatesArray = new Set();
        } else {
            for (var obj of this.selectedStatesArray) {
                if (obj.value == toRemove)
                    this.selectedStatesArray.delete(obj);
            }
        }




        this.selectedStates = '';
        for (var pick of this.selectedStatesArray) {
            if (this.selectedStates.length === 0) {
                this.selectedStates = this.selectedStates + pick.value;


            } else if (this.selectedStates.length > 0) {
                this.selectedStates = this.selectedStates + ';' + pick.value;
            }

        }
        // this.selectedId = '';
        // for (var pick of this.selectIdList) {
        //     if (this.selectedId.length === 0) {
        //         this.selectedId = this.selectedId + pick;


        //     } else if (this.selectedId.length > 0) {
        //         this.selectedId = this.selectedId + ';' + pick;
        //     }

        // }

        this.checkIsAllChecked();
        const updateFieldEvent = new CustomEvent('updatefielvalue', {
            detail: { fieldName: this.fieldName, fieldValue: this.selectedStates }
        });
        this.dispatchEvent(updateFieldEvent);
        this.showPicklist = true;
    }

    handlePicklistRemove(event) {
        let toRemove = event.currentTarget.getAttribute('data-picklist');
        //console.log('toRemove',JSON.stringify(toRemove));
        //console.log('this.selectedStatesArray',/*JSON.stringify*/(this.selectedStatesArray));
        /*const findIndex = this.selectedStatesArray.findIndex(a => a.value === toRemove)

         findIndex !== -1 && this.selectedStatesArray.splice(findIndex , 1)*/
        for (var obj of this.selectedStatesArray) {
            if (obj.value == toRemove)
                this.selectedStatesArray.delete(obj);
        }
        //console.log('this.selectedStatesArray111',/*JSON.stringify*/(this.selectedStatesArray));


        this.selectedStates = '';
        for (var pick of this.selectedStatesArray) {
            if (this.selectedStates.length === 0) {
                this.selectedStates = this.selectedStates + pick.value;


            } else if (this.selectedStates.length > 0) {
                this.selectedStates = this.selectedStates + ';' + pick.value;
            }

        }
        //console.log('this.pastStates',this.pastStates);




        // this.selectIdList.delete(pickValueToLableMap.get(toRemove));
        // this.selectedId = '';
        // for (var pick of this.selectIdList) {
        //     if (this.selectedId.length === 0) {
        //         this.selectedId = this.selectedId + pick;


        //     } else if (this.selectedId.length > 0) {
        //         this.selectedId = this.selectedId + ';' + pick;
        //     }

        // }

        this.checkIsAllChecked();
        this.showPicklist = false;
        const updateFieldEvent = new CustomEvent('updatefielvalue', {
            detail: { fieldName: this.fieldName, fieldValue: (this.selectedStates) }
        });
        this.dispatchEvent(updateFieldEvent);
    }

    /*disconnectedCallback() {
        unregisterAllListeners(this);
    }*/

    handleParentChange() {
        this.selectedStatesArray = new Set();
        this.selectedStates = '';
        this.selectIdList = new Set();
        this.selectedId = '';
    }

    /*connectedCallback() {
      registerListener('parentchanged', this.handleParentChange, this);


        let pickValueToLableMap = new Map();

        if (this.picklistValues) {
            console.log('this.picklistValues',JSON.stringify(this.picklistValues));
            let picklists = [];
            picklists.push({ label: 'All', value: 'All' })
            for (let val of this.picklistValues) {
                picklists.push({ label: val.label, value: val.value })
            }
            this.pastStates = picklists;
            if (this.selectedValues) {
                this.selectedStatesArray = new Set();
                for (var value of this.selectedValues.split(';')) {
                    let temp = picklists[picklists.findIndex(a => a.value == value)];
                    this.selectedStatesArray.add(temp);
                }
                this.selectedStates = this.selectedValues;
    
            }

            this.checkIsAllChecked();
        } else {

            fatchPickListValue({ objInfo: { 'sobjectType': this.objectName }, picklistFieldApi: this.fieldName }).then(result => {


                let picklists = [];
                picklists.push({ label: 'All', value: 'All' })
                for (let val of result) {
                    picklists.push({ label: val.label, value: val.value })
                    pickValueToLableMap.set(val.value ,val.label );
                }

                this.pastStates = picklists;
                if (this.selectedValues) {
                    this.selectedStatesArray = new Set();
                    for (var value of this.selectedValues.split(';')) {
                        this.selectedStatesArray.add(pickValueToLableMap.get(value));
                    }
                    this.selectedStates = this.selectedValues;
                }

                this.checkIsAllChecked();
            })
                .catch(error => {
                    console.log(error);

                });
        }


    }*/

    @api
    resetPicklist(picklist) {
        if (this.selectedValues) {
            for (var value of this.selectedValues.split(';')) {
                this.selectedStatesArray.add(value);
            }
            this.selectedStates = this.selectedValues;

        }
        // if (this.selectedValues) {
        //     for (var value of this.selectedValues.split(';')) {
        //         this.selectIdList.add(value);
        //     }
        //     this.selectedStates = this.selectedValues;

        // }
        let picklists = [];
        picklists.push({ label: 'All', value: 'All' })
        for (let val of picklist) {
            picklists.push({ label: val.label, value: val.value })
        }

        this.pastStates = picklists;

        this.checkIsAllChecked();
    }

    checkIsAllChecked() {
        let allFieldCount = this.pastStates.length - 1;
        let selectedFieldCount = this.selectedStatesArray.size;

        // console.log(allFieldCount);
        // console.log(selectedFieldCount);
        // console.log(allFieldCount == selectedFieldCount ? true : false);

        this.isAllSelected = allFieldCount == selectedFieldCount ? true : false;

    }

    @track navigatorIndex = [];

    handleKeyPress(event) {
        // console.log('up down: ', event.keyCode);
        if (event.keyCode == 13 || event.keyCode == 32) {
            this.handleShowPicklist();
        }
    }
    handleKeyDown(event) {
        if (event.keyCode == 40) {
            this.navigatorIndex = [];
            console.log('down');
            this.navigatorIndex.push(this.template.querySelector('li'));
            let lookups = this.template.querySelectorAll('c-nh-qris-each-lookup');
            console.log(lookups.length);

            lookups.forEach(element => {
                element.handleArrowKeys();
            });

            this.navigatorIndex.forEach(element => {
                
            });
            this.navigatorIndex[0].focus();
            // console.log('this.navigatorIndex: ', this.navigatorIndex.length);
        } else if (event.keyCode == 38) {
            // console.log('up');
            // if (this.navigatorIndex > 0)
            //     this.navigatorIndex--;
        }

        // for (i = 0; i < this.pastStates.length; i++) {
        //     if (i === this.navigatorIndex) {
        //         this.pastStates[i].selected = true;
        //     }
        //     else {
        //         this.pastStates[i].selected = false;
        //     }
        // }
    }

    handleArrowEvent(event) {
        // console.log(event.detail);
        this.navigatorIndex.push(event.detail);
        // console.log('this.navigatorIndex: ', this.navigatorIndex.length);
    }
}